package reclists;

public class Pair
{
	String a;
	String b;
	
	public Pair(String a, String b)
	{
		this.a=a;
		this.b=b;
	}
	
	public String fst()
	{
		return a;
	}
	
	public String snd()
	{
		return b;
	}
	
	public boolean equalTo(Pair other)
	{
		if(a==other.fst())
		{
			if(b==other.snd())
			{
				return true; 
			}
		}
		else if(a==other.snd())
		{
			if(b==other.fst())
			{
				return true;
			}
		}
		return false;
	}
	
	boolean equals(Pair other)
	{
		return a==other.fst() && b==other.snd();
	}
	
	public String toString()
	{
		return "(" + a + ", " + b + ")";
	}
	
}