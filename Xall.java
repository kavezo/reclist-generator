package reclists;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Stack;

public class Xall implements Generator
{
	static String[] phonemes;
	static HashMap<String, Integer> phoncounts = new HashMap<String, Integer>();

	@Override
	public LinkedList<Pair> getGraph(String file) 
	{
		phonemes = file.split(" ");
		
		LinkedList<Pair> graph = new LinkedList<Pair>();
		
		for(String phoneme0:phonemes)
		{
			for(String phoneme1:phonemes)
			{
				if(phoneme0!=phoneme1) //because...loops aren't really that useful...?
				{
					Pair edge = new Pair(phoneme0, phoneme1);
					graph.add(edge);
				}
			}
			phoncounts.put(phoneme0, 0);
		}
		return graph;
	}

	@Override
	public String getPath(LinkedList<Pair> graph) 
	{
		//This method is purely Eulerian circuit finding.
		String path = "";
		String currph = phonemes[0];
		Stack<String> found = new Stack<String>();
		
		String[] connecteds = GraphUtil.mergesort(GraphUtil.getDirectlyConnected(graph, currph), phoncounts);
		
		do
		{
			if(connecteds.length==0)
			{
				path = currph + " " + path;
				currph = found.pop();
			}
			else
			{
				Pair removeme = null;
				found.push(currph);
				for(Pair edge:graph)
				{
					if(edge.equals(new Pair(currph, connecteds[0])))
					{
						removeme = edge;
					}
				}
				graph.remove(removeme);
				currph = connecteds[0];
			}
			connecteds = GraphUtil.mergesort(GraphUtil.getDirectlyConnected(graph, currph), phoncounts);
		}while(!found.empty());
		
		return phonemes[0] + " " + path;
	}

	@Override
	public String[] divide(int length, String path) 
	{
		//Same thing as always, but doesn't worry about syllables or consonant/vowel problems.
		String[] phonemes = path.split(" ");
		LinkedList<String> sylex = new LinkedList<String>();
		int i=0;
		
		while(i<phonemes.length-1)
		{
			sylex.add(phonemes[i]);
			if(sylex.size()%length == length-1)
			{
				sylex.add(phonemes[i+1]);
			}
			i++;
		}
		
		String[] reclist = new String[(int) (Math.ceil(sylex.size()/(length*1.0)))];
		
		for(i=0; i<reclist.length; i++)
		{
			String wave = "";
			for(int j=0; j<length; j++)
			{
				try{
				wave += sylex.remove();}
				catch(NoSuchElementException e)
				{
					wave += "";
				}
			}
			reclist[i] = wave;
		}
		
		return reclist;
	}
}
