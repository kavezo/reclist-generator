package reclists;


import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

public class Frame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 4522986467598162317L;

	//This class has the worst code in the entire thing. When it comes to Swing, I'm utterly lost :/
	public static void main(String[] args) 
	{
		Frame frame = new Frame();
		JTabbedPane tabs = new JTabbedPane();
		tabs.addTab("CVVC", frame.getPanel1("CVVC"));
		tabs.addTab("VCV", frame.getPanel1("VCV")); //bc the first two tabs are pretty much the same, but buttons do different things
		tabs.addTab("X*", frame.getPanel2());
		tabs.addTab("CV", frame.getPanel3());
		frame.add(tabs);
		
		frame.setTitle("Reclist Generator");
		frame.setSize(600,160); //Please don't resize :(
		frame.repaint();
		frame.setVisible(true);
	}
	
	//I have no idea what I'm doing!!
	JComponent getPanel1(String command)
	{
		JPanel panel = new JPanel();
		JTextField textfield1 = new JTextField(25);
		textfield1.setPreferredSize(new Dimension(100, 20));
		JTextField textfield2 = new JTextField(25);
		textfield2.setPreferredSize(new Dimension(80, 20));
		
		SpinnerNumberModel model = new SpinnerNumberModel();
		model.setMinimum(2); //bc the divide algorithm can't handle <2
		model.setValue(5); //start at 5
		JSpinner spinner = new JSpinner(model);
		
		JButton button = new JButton("Generate " + command + " reclist");
		button.setActionCommand(command);
		button.addActionListener(this);
		
		panel.add(new JLabel("Vowels (space-separated):"));
		panel.add(textfield1);
		panel.add(new JLabel("Consonants (space-separated):"));
		panel.add(textfield2);
		panel.add(new JLabel("Number of syllables:"));
		panel.add(spinner);
		panel.add(button);
		
		return panel;
	}
	
	JComponent getPanel2()
	{
		JPanel panel = new JPanel();
		JTextField textfield = new JTextField(25);
		textfield.setPreferredSize(new Dimension(100, 20));
		
		SpinnerNumberModel model = new SpinnerNumberModel();
		model.setMinimum(2); //bc the divide algorithm can't handle lengths < 2
		model.setValue(5); //start at 5
		JSpinner spinner = new JSpinner(model);
		
		JButton button = new JButton("Generate reclist");
		button.setActionCommand("X*");
		button.addActionListener(this);
		
		panel.add(new JLabel("Phonemes (space-separated):"));
		panel.add(textfield);
		panel.add(new JLabel("Number of syllables:"));
		panel.add(spinner);
		panel.add(button);
		
		return panel;
	}
	
	JComponent getPanel3()
	{
		JPanel panel = new JPanel();
		JTextField textfield1 = new JTextField(25);
		textfield1.setPreferredSize(new Dimension(100, 20));
		JTextField textfield2 = new JTextField(25);
		textfield2.setPreferredSize(new Dimension(80, 20));
		
		JButton button = new JButton("Generate reclist");
		button.setActionCommand("CV");
		button.addActionListener(this);
		
		panel.add(new JLabel("Vowels (space-separated):"));
		panel.add(textfield1);
		panel.add(new JLabel("Consonants (space-separated):"));
		panel.add(textfield2);
		panel.add(button);
		
		return panel;
	}
	
	void showReclist(Generator g, String file, int limit)
	{
		String[] reclist = g.divide(limit, g.getPath(g.getGraph(file)));
		String show = "";
		for(int i=0; i<reclist.length; i++)
		{
			show += reclist[i] + "\n";
		}
		
		JFrame dialog = new JFrame("Reclist");
		JTextArea area = new JTextArea(8,15);
		JScrollPane scrollPane = new JScrollPane(area); 
		area.setEditable(false);
		area.setText(show);
		dialog.add(scrollPane);
		dialog.setSize(300, 300);
		dialog.repaint();
		dialog.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) 
	{
		JPanel panel = (JPanel) ((JButton) e.getSource()).getParent();
				
		//UNSAFE CAST PARTY!! WOOT!!!
		if(e.getActionCommand().equals("CVVC"))
		{
			String vowels = ((JTextField)(panel.getComponent(1))).getText();
			String consonants = ((JTextField)(panel.getComponent(3))).getText();
			if(vowels=="" || consonants =="")
				JOptionPane.showMessageDialog(new JFrame(), "You can't generate a reclist with no vowels or no consonants.");
			else if(vowels.contains("  ") || consonants.contains("  "))
				JOptionPane.showMessageDialog(new JFrame(), "You accidentally entered a double space.");
			else
			{
				String file = vowels + "\n" + consonants;
				int limit = (Integer) ((JSpinner)(panel.getComponent(5))).getValue();
				showReclist(new CVVC(), file, limit);
			}
		}
		else if(e.getActionCommand().equals("VCV"))
		{
			String vowels = ((JTextField)(panel.getComponent(1))).getText();
			String consonants = ((JTextField)(panel.getComponent(3))).getText();
			if(vowels=="" || consonants =="")
				JOptionPane.showMessageDialog(new JFrame(), "You can't generate a reclist with no vowels or no consonants.");
			else if(vowels.contains("  ") || consonants.contains("  "))
				JOptionPane.showMessageDialog(new JFrame(), "You accidentally entered a double space.");
			else
			{
				String file = vowels + "\n" + consonants;
				int limit = (Integer) ((JSpinner)(panel.getComponent(5))).getValue();
				showReclist(new VCV(), file, limit);
			}
		}
		else if(e.getActionCommand().equals("X*"))
		{
			String phonemes = ((JTextField)(panel.getComponent(1))).getText();
			if(phonemes=="")
				JOptionPane.showMessageDialog(new JFrame(), "You can't generate a reclist with no phonemes.");
			else if(phonemes.contains("  "))
				JOptionPane.showMessageDialog(new JFrame(), "You accidentally entered a double space.");
			else
			{
				int limit = (Integer) ((JSpinner)(panel.getComponent(3))).getValue();
				showReclist(new Xall(), phonemes, limit);
			}
		}
		else if(e.getActionCommand().equals("CV"))
		{
			String vowelline = ((JTextField)(panel.getComponent(1))).getText();
			String consonantline = ((JTextField)(panel.getComponent(3))).getText();
			if(vowelline=="" || consonantline =="")
				JOptionPane.showMessageDialog(new JFrame(), "You can't generate a reclist with no vowels or no consonants.");
			else if(vowelline.contains("  ") || consonantline.contains("  "))
				JOptionPane.showMessageDialog(new JFrame(), "You accidentally entered a double space.");
			else
			{
				String[] vowels = vowelline.split(" ");
				String[] consonants = ("_ " + consonantline).split(" ");
				String[] reclist = new String[vowels.length*(consonants.length)];
				int i=0;
				for(String consonant:consonants)
				{
					for(String vowel:vowels)
					{
						reclist[i] = (consonant.equals("_")?"":consonant)+vowel;
						i++;
					}
				}
				String show = "";
				for(i=0; i<reclist.length; i++)
				{
					show += reclist[i] + "\n";
				}
				
				JFrame dialog = new JFrame("Reclist");
				JTextArea area = new JTextArea(8,15);
				JScrollPane scrollPane = new JScrollPane(area); 
				area.setEditable(false);
				area.setText(show);
				dialog.add(scrollPane);
				dialog.setSize(300, 300);
				dialog.repaint();
				dialog.setVisible(true);
			}
		}
	}	

}
