package reclists;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Stack;

public class CVVC implements Generator{

	static String[] vowels;
	static String[] consonants;
	static HashMap<String, Integer> phoncounts = new HashMap<String, Integer>(); //number of times each node has been visited
	
	public LinkedList<Pair> getGraph(String file)//"file", because I used a file at first. Too lazy to refactor because of all the unrefactorable instances.
	{
		vowels = file.split("\n")[0].split(" ");
		consonants = file.split("\n")[1].split(" ");
		
		LinkedList<Pair> graph = new LinkedList<Pair>();
		
		for(String vowel0:vowels)//Only adds the vowels to the graph, because consonants are dealt with much more easily
		{
			for(String vowel1:vowels)
			{
				if(!vowel0.equals(vowel1))
				{
					Pair edge = new Pair(vowel0, vowel1);
					graph.add(edge); //CVVC doesn't require two of the same phoneme in a row
					phoncounts.put(vowel0, 0);
				}
			}
		}
		return graph;
	}
	
	public String getPath(LinkedList<Pair> graph)
	{
		String path = "";
		String currph = vowels[0];
		Stack<String> found = new Stack<String>();
		
		String[] connecteds = GraphUtil.mergesort(GraphUtil.getDirectlyConnected(graph, currph), phoncounts);
							//Get the connected nodes, sorted in order of least to most visited.
		do //this loop generates the Eulerian circuit
		{
			if(connecteds.length==0)
			{
				path = currph + " " + path;
				currph = found.pop();
			}
			else
			{
				Pair removeme = null;
				found.push(currph);
				for(Pair edge:graph)
				{
					if(edge.equals(new Pair(currph, connecteds[0])))
					{
						removeme = edge;
					}
				}
				graph.remove(removeme);
				currph = connecteds[0];
			}
			connecteds = GraphUtil.mergesort(GraphUtil.getDirectlyConnected(graph, currph), phoncounts);
		}while(!found.empty());
		
		path = vowels[0] + " " + path; //bc for some reason the first node won't cooperate.
		
		for(String c:consonants) //O(n) thing to take care of consonants :D
		{
			for(int i=0; i<vowels.length; i++)
			{
				path += c + " " + vowels[i] + " ";
			}
		}
		
		return path;
	}
	
	@Override
	public String[] divide(int length, String path)//This method is combinatoric black magic.
	{
		String[] phonemes = path.split(" ");
		LinkedList<String> sylex = new LinkedList<String>();
		int i=0;
		
		/*
		 So every [length] syllables in the path, we have to cut it and put it as a separate
		 thing to record. But that erases the link between the last phoneme we cut and the next
		 one in the list, rendering the reclist incomplete. Thus, every [length] syllables, we
		 re-add the phoneme.
		 E.g. length = 3, path = "a ba ca da e fe ge"
		 The first one after 3 is "da", so we add "d" to the phoneme list and get "abacad"
		 instead of "abaca" for the first thing to record.
		 */
		while(i<phonemes.length)
		{
			String syllable = "";
			if(contains(consonants, phonemes[i])) //because it's much easier to put together syllables than to take them apart
			{
				syllable = phonemes[i]+phonemes[i+1];
				i++;
			}
			else
			{
				syllable = phonemes[i];
			}
			if(sylex.size()%length == length-1 && i<phonemes.length-1)
			{//if it's a consonant and we add it to the end, the syllable number doesn't change,
				//but it does if it's a vowel. So different behaviors based off of this.
				if(contains(consonants,phonemes[i+1]))
				{
					syllable += phonemes[i+1];
					sylex.add(syllable);
					i++;
					continue;
				}
				else
				{
					sylex.add(syllable);
					continue;
				}					
			}
			sylex.add(syllable); //if we don't hit one of the "continue"'s, we haven't added the syllable yet
			i++;			
		}
		
		String[] reclist = new String[(int) (Math.ceil(sylex.size()/(length*1.0)))];
		
		for(i=0; i<reclist.length; i++)
		{
			String wave = "";
			for(int j=0; j<length; j++) //actually adding the things to the reclist
			{
				try{
				wave += sylex.remove();}
				catch(NoSuchElementException e)
				{
					wave += "";
				}
			}
			reclist[i] = wave;
		}
		
		return reclist;
	}
	static boolean contains(String[] list, String str)
	{
		boolean returnme = false;
		for(String thing:list)
		{
			if(thing.equals(str))
			{
				returnme = true;
			}
		}
		return returnme;
	}
	
}
