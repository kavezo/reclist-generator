package reclists;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Stack;

public class VCV implements Generator{
	
	static String[] vowels;
	static String[] consonants;
	static HashMap<String, Integer> phoncounts = new HashMap<String, Integer>();

	//See comments for the respective methods of CVVC. This is practically the same.
	
	@Override
	public LinkedList<Pair> getGraph(String file) //Pretty much same thing as CVVC, but adds loops to the graph.
	{
		vowels = file.split("\n")[0].split(" ");
		consonants = file.split("\n")[1].split(" ");
		
		LinkedList<Pair> graph = new LinkedList<Pair>();
		
		for(String vowel0:vowels)
		{
			for(String vowel1:vowels)
			{
				Pair edge = new Pair(vowel0, vowel1);
				graph.add(edge);
				phoncounts.put(vowel0, 0);
			}
		}
		return graph;
	}

	@Override
	public String getPath(LinkedList<Pair> graph)
	{
		String path = "";
		String currph = vowels[0];
		Stack<String> found = new Stack<String>();
		
		String[] connecteds = GraphUtil.mergesort(GraphUtil.getDirectlyConnected(graph, currph), phoncounts);
		
		do
		{
			if(connecteds.length==0)
			{
				path = currph + " " + path;
				currph = found.pop();
			}
			else
			{
				Pair removeme = null;
				found.push(currph);
				for(Pair edge:graph)
				{
					if(edge.equals(new Pair(currph, connecteds[0])))
					{
						removeme = edge;
					}
				}
				graph.remove(removeme);
				currph = connecteds[0];
			}
			connecteds = GraphUtil.mergesort(GraphUtil.getDirectlyConnected(graph, currph), phoncounts);
		}while(!found.empty());
		
		path = vowels[0] + " " + path;
		String[] syllables = path.split(" ");
		
		for(String c:consonants)
		{
			for(int i=1; i<syllables.length; i++)
			{
				path += c + " " + syllables[i] + " ";
			}
		}
		
		return path;
	}

	@Override
	public String[] divide(int length, String path) 
	{
		String[] phonemes = path.split(" ");
		LinkedList<String> sylex = new LinkedList<String>();
		int i=0;
		
		while(i<phonemes.length)
		{
			if(CVVC.contains(consonants, phonemes[i]))
			{
				sylex.add(phonemes[i]+phonemes[i+1]);
				i++;
			}
			else
			{
				sylex.add(phonemes[i]);
			}
			if(sylex.size()%length == 0)
			{
				sylex.add(phonemes[i]);
			}
			i++;
		}
		
		/*
		for(int i=0; i<syllables.length; i++)
		{
			sylex.add(syllables[i]);
			if(sylex.size()%length == length-1 && i<syllables.length-1)
			{
				if(CVVCdir.contains(vowels, syllables[i+1]))
				{
					sylex.add(syllables[i+1]);
				}
				else
				{
					sylex.add(syllables[i+1].substring(0,1));
				}
			}
		}
		*/
		
		String[] reclist = new String[(int) (Math.ceil(sylex.size()/(length*1.0)))];
		
		for(i=0; i<reclist.length; i++)
		{
			String wave = "";
			for(int j=0; j<length; j++)
			{
				try{
				wave += sylex.remove();}
				catch(NoSuchElementException e)
				{
					wave += "";
				}
			}
			reclist[i] = wave;
		}
		
		return reclist;
	}

}
