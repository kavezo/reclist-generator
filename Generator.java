package reclists;

import java.util.LinkedList;

interface Generator 
{
	LinkedList<Pair> getGraph(String file);
	String getPath(LinkedList<Pair> graph);
	String[] divide(int length, String path);
}
