package reclists;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class GraphUtil {

	static void printgraph(LinkedList<Pair> graph)
	{
		for(Pair pair:graph)
		{
			System.out.println("(" + pair.fst() + ", " + pair.snd() + ")");
		}
	}
	static boolean isBridge(LinkedList<Pair> graph, Pair edge)
	{
		LinkedList<Pair> copy = new LinkedList<Pair>();
		ArrayList<String> nodes = new ArrayList<String>();
		for(Pair e:graph)
		{
			copy.add(e);
			if(!nodes.contains(e.fst()))
			nodes.add(e.fst());
			if(!nodes.contains(e.snd()))
			nodes.add(e.snd());
		}
		
		copy.remove(edge);
		
		HashSet<String> found = new HashSet<String>();
		Stack<String> toExplore = new Stack<String>();
		toExplore.push(nodes.get(0));
		
		while(!toExplore.empty())
		{
			String node = toExplore.pop();
			if(!found.contains(node) && !toExplore.contains(node))
			{
				found.add(node);
				String[] connecteds = getDirectlyConnected(copy, node);
				for(String connode:connecteds)
				{
					if(!found.contains(connode))
					{
						toExplore.push(connode);
					}
				}
			}
		}
		
		return found.size()!=nodes.size();
	}
	static String[] getDirectlyConnected(LinkedList<Pair> graph, String node)
	{
		List<String> connecteds = new ArrayList<String>();
		for(int i=0; i<graph.size(); i++)
		{
			if(graph.get(i).fst().equals(node))
			{
				connecteds.add(graph.get(i).snd());
			}
		}
		String[] returnme = connecteds.toArray(new String[0]);
		return returnme;
	}
	static String[] mergesort(String[] list, HashMap<String, Integer> phoncounts)
	{
		if(list.length<=1)
		{
			return list;
		}
		else
		{
			String[][] splits = splitList(list);
			String[] a = mergesort(splits[0], phoncounts);
			String[] b = mergesort(splits[1], phoncounts);
			String[] sorted = new String[a.length+b.length];
			int aind = 0;
			int bind = 0;
			for(int i=0; i<sorted.length; i++)
			{
				if(!(aind>=a.length)&&!(bind>=b.length))
				{
					if(phoncounts.get(a[aind])<=phoncounts.get(b[bind]))
					{
						sorted[i] = a[aind];
						aind++;
					}
					else
					{
						sorted[i] = b[bind];
						bind++;
					}
				}
				else if(aind>=a.length)
				{
					sorted[i] = b[bind];
					bind++;
				}
				else if(bind>=a.length)
				{
					sorted[i] = a[aind];
					aind++;
				}
			}
			return sorted;
		}
	}
	
	static String[][] splitList(String[] list)
	{
		int middle = (int) Math.ceil(list.length/2);
		String[] a = new String[middle];
		for(int i=0; i<middle; i++)
		{
			a[i] = list[i];
		}
		String[] b = new String[list.length-middle];
		for(int i=middle; i<list.length; i++)
		{
			b[i-middle] = list[i];
		}
		String[][] returnme = {a,b};
		return returnme;
	}

}
